let Options = {}

function setupOptions(options) {
	Options = Object.assign(Options, options);
}

function get(key) {
	return Options[key];
}

function App(options) {
	return {get};
}

export default App;
