CI
###

Install docker

```
sudo aptitude install docker.io
```

Install gitlab-runner from https://packages.gitlab.com/runner/gitlab-runner

```
wget --content-disposition https://packages.gitlab.com/runner/gitlab-runner/packages/debian/buster/gitlab-runner_10.5.0_amd64.deb/download.deb
sudo dpkg -i gitlab-runner_10.5.0_amd64.deb
```

Register gitlab-runner

```
CI_TOKEN="YOUR TOKEN"

sudo gitlab-runner register \
    --non-interactive \
    --url "https://gitlab.com/" \
    --registration-token "$CI_TOKEN" \
    --description "Gitlab runner for X" \
    --executor "docker" \
    --docker-image "alpine:latest"
```

example config
```
concurrent = 1
check_interval = 0
log_level = "debug"

[[runners]]
  name = "debian-home-simon"
  url = "https://gitlab.com/"
  token = "72f22336bf03fc506645baea574150"
  executor = "docker"

  [runners.docker]
    tls_verify = false
    image = "alpine:latest"
    privileged = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0

  [runners.cache]
```
