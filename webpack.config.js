const webpack = require("webpack");
const path = require("path");
const UglifyJSPlugin = require("uglifyjs-webpack-plugin");
const DuplicatePackageCheckerPlugin = require("duplicate-package-checker-webpack-plugin");

const ENV = process.env.NODE_ENV || "development";
const DEV_MODE = ENV === "development";
const PROD_MODE = ENV === "production";

module.exports = {
	context: path.resolve(__dirname, "src"),
	mode: ENV,
	entry: {
		app: "./index.js"
	},
	output: {
		path: path.resolve(__dirname, "dist"),
		filename: "[name].js",
		library: "kogamaApp",
		libraryExport: "default",
		publicPath: "/"
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				use: ["source-map-loader"],
				enforce: "pre"
			},
			{
				test: /\.js?$/,
				loader: "babel-loader",
				exclude: /node_modules/,
				include: path.resolve(__dirname, "src/"),
				options: {
					"presets": [
						["@babel/env", {
							"modules": false,
							"loose": false,
							"targets": {
								"browsers": ["last 2 versions", "IE >= 9"]
							}
						}]
					],
					"plugins": [
						"@babel/plugin-proposal-class-properties",
						"@babel/plugin-proposal-object-rest-spread"
					],
					babelrc: false
				}
			}]
	},
	plugins: [
		new webpack.DefinePlugin({
			"process.env.NODE_ENV": JSON.stringify(ENV)
		}),
	].concat(DEV_MODE ? [] : [
		// build will fail when there are errors while compiling. Even eslint
		// errors cause build failure.
		new webpack.NoEmitOnErrorsPlugin(),

		// warn on duplicate packages in build
		new DuplicatePackageCheckerPlugin()
	]),
	optimization: {
		minimizer: [
			new UglifyJSPlugin({
				sourceMap: true,
				uglifyOptions: {
					mangle: {
						// mangle properties doesn't work
						//properties: {
						//	keep_quoted: true,
						//	reserved: ["Backbone", "$", "_"]
						//}
					},
					compress: {
						booleans: true,
						collapse_vars: true,
						comparisons: true,
						conditionals: true,
						dead_code: true,
						drop_console: true,
						drop_debugger: true,
						evaluate: true,
						hoist_funs: true,
						ie8: true,
						if_return: true,
						join_vars: true,
						keep_fargs: false,
						loops: false, // possibly true
						passes: 2,
						properties: true,
						pure_getters: true,
						reduce_vars: true,
						unsafe: true,
						unsafe_comps: true,
						unused: true,
						warnings: false
					}
				}
			})
		]
	},
	stats: { colors: true },
	devtool: PROD_MODE ? "source-map" : "cheap-module-eval-source-map"
};
